//
//  AlertView.swift
//  BD Project
//
//  Created by Nazar Lysak on 09.12.2021.
//

import Foundation
import UIKit

class AlertView {
    
    func showAlert(title: String? = nil, message: String? = nil, actionTitle: String? = nil) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: .cancel, handler: nil)
        
        alert.addAction(action)
        
        return alert
    }
}
