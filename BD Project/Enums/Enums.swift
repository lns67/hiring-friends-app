//
//  Enums.swift
//  BD Project
//
//  Created by Nazar Lysak on 09.12.2021.
//

import Foundation

enum Requests: String {
    case request1 = "Request 1"
    case request2 = "Request 2"
    case request3 = "Request 3"
    case request4 = "Request 4"
    case request5 = "Request 5"
    case request6 = "Request 6"
    case request7 = "Request 7"
    case request8 = "Request 8"
    case request9 = "Request 9"
    case request10 = "Request 10"
    case request11 = "Request 11"
    case request12 = "Request 12"
}
