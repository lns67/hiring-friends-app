//
//  NetworkManager.swift
//  BD Project
//
//  Created by Nazar Lysak on 12.12.2021.
//

import Foundation
import Alamofire

// MARK: - NetworkManager
class NetworkManager {
    
    static let shared = NetworkManager()
    
    func networkRequest(parametrs: [String : Any]? = nil,
                        method: HTTPMethod,
                        headers: [String : String]? = nil,
                        urtlPath: String,
                        completionHandler: @escaping (Data?, String?) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable ?? false {
            
            AF.request(urtlPath,
                       method: method,
                       parameters: parametrs).response { response in
                
                switch response.result {
                case .success(let data):
                    completionHandler(data, nil)
                case .failure(let error):
                    completionHandler(nil, error.localizedDescription)
                }
            }
        } else {
            completionHandler(nil, "No internet connection!")
        }
    }
}
