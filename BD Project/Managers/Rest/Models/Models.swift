//
//  Models.swift
//  BD Project
//
//  Created by Nazar Lysak on 12.12.2021.
//

import Foundation

// MARK: - Models

struct Clients: Codable {
    let id: Int
    let firstName: String
    let lastName: String
}

struct Donations: Codable {
    let id: Int
    let hired_friend_id: Int
    let client_id: Int
    let gift_id: Int
}

struct Festivals: Codable {
    let id: Int
    let name: String
    let date: String
    let address: String
}

struct Gifts: Codable {
    let id: Int
    let name: String
}

struct Groups: Codable {
    let id: Int
    let name: String
}

struct HiredFriends: Codable {
    let id: Int
    let firstName: String
    let lastName: String
//    let groupId: Int
}

struct HiringFestivals: Codable {
    let id: Int
    let group_id: Int
    let client_id: Int
    let festival_id: Int
}

struct HiringMeetings: Codable {
    let id: Int
    let hired_friend_id: Int
    let client_id: Int
    let meeting_id: Int
}

struct Holidays: Codable {
    let id: Int
    let date: String
    let hired_friend_id: Int
}

struct Meetings: Codable {
    let id: Int
    let name: String
    let date: String
    let address: String
}

struct Request1: Codable {
    let first_name: String
}

struct Request3: Codable {
    let name: String
}

struct Request6: Codable {
    let month: String
    let count: String
}

struct Request11: Codable {
    let first_name: String
    let date: String
}
