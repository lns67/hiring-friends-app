//
//  Rest.swift
//  BD Project
//
//  Created by Nazar Lysak on 12.12.2021.
//

import Foundation

// MARK: - Rest
class Rest {
    
    func loadClients(completionHandler: @escaping ([Clients]?, String?) -> Void) {
                
        NetworkManager.shared.networkRequest(method: .get, urtlPath: URLs.urlGetClients) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode([Clients].self, from: data)
            completionHandler(response, nil)
        }
    }
    
    func loadHiredFriends(completionHandler: @escaping ([HiredFriends]?, String?) -> Void) {
                
        NetworkManager.shared.networkRequest(method: .get, urtlPath: URLs.urlGetHiredFriends) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode([HiredFriends].self, from: data)
            completionHandler(response, nil)
        }
    }
    
    func loadMeetings(completionHandler: @escaping ([Meetings]?, String?) -> Void) {
                
        NetworkManager.shared.networkRequest(method: .get, urtlPath: URLs.urlGetMeetings) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode([Meetings].self, from: data)
            completionHandler(response, nil)
        }
    }
    
    func loadRequest1(clientId: Int, number: Int, dateStart: String, dateEnd: String, completionHandler: @escaping ([Request1]?, String?) -> Void) {
                
        let parametrs: [String : Any] = ["clientId" : clientId, "number" : number,
                                            "dateStart" : dateStart, "dateEnd" : dateEnd]
        
        NetworkManager.shared.networkRequest(parametrs: parametrs, method: .get, urtlPath: URLs.urlRequest1) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode([Request1].self, from: data)
            completionHandler(response, nil)
        }
    }
        
    func loadRequest2(hiredFriendId: Int, number: Int, dateStart: String, dateEnd: String, completionHandler: @escaping ([Request1]?, String?) -> Void) {
        
        let parametrs: [String : Any] = ["hiredFriendId" : hiredFriendId, "number" : number,
                                            "dateStart" : dateStart, "dateEnd" : dateEnd]

        NetworkManager.shared.networkRequest( parametrs: parametrs, method: .get, urtlPath: URLs.urlRequest2) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode([Request1].self, from: data)
            completionHandler(response, nil)
        }
    }

    func loadRequest3(hiredFriendId: Int, number: Int, dateStart: String, dateEnd: String, completionHandler: @escaping ([Request3]?, String?) -> Void) {
        
        let parametrs: [String : Any] = ["hiredFriendId" : hiredFriendId, "number" : number,
                                            "dateStart" : dateStart, "dateEnd" : dateEnd]

        NetworkManager.shared.networkRequest( parametrs: parametrs, method: .get, urtlPath: URLs.urlRequest3) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode([Request3].self, from: data)
            completionHandler(response, nil)
        }
    }
    
    func loadRequest6(completionHandler: @escaping ([Request6]?, String?) -> Void) {

        NetworkManager.shared.networkRequest(method: .get, urtlPath: URLs.urlRequest6) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode([Request6].self, from: data)
            completionHandler(response, nil)
        }
    }
    
    func loadRequest10(hiredFriendId: Int, clientId: Int, dateStart: String, dateEnd: String, completionHandler: @escaping ([Request3]?, String?) -> Void) {
        
        let parametrs: [String : Any] = ["clientId": clientId, "hiredFriendId" : hiredFriendId,
                                            "dateStart" : dateStart, "dateEnd" : dateEnd]

        NetworkManager.shared.networkRequest( parametrs: parametrs, method: .get, urtlPath: URLs.urlRequest10) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode([Request3].self, from: data)
            completionHandler(response, nil)
        }
    }
    
    func loadRequest11(startHiredFriendId: Int, endHiredFriendId: Int, completionHandler: @escaping ([Request11]?, String?) -> Void) {
        
        let parametrs: [String : Any] = ["startHiredFriendId": startHiredFriendId, "endHiredFriendId" : endHiredFriendId]

        NetworkManager.shared.networkRequest( parametrs: parametrs, method: .get, urtlPath: URLs.urlRequest11) { data, error in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error!")
                return
            }
            
            let decoder = JSONDecoder()
            let response = try? decoder.decode([Request11].self, from: data)
            completionHandler(response, nil)
        }
    }
}
