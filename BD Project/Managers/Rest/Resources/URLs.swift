//
//  URLs.swift
//  BD Project
//
//  Created by Nazar Lysak on 12.12.2021.
//

import Foundation

// MARK: - URLs
struct URLs {
    
    static let url1 = "http://localhost:8001/hiring-meetings"
    
    static let urlGetClients = "http://localhost:8001/clients"
    static let urlGetHiredFriends = "http://localhost:8001/hired-friends"
    static let urlGetMeetings = "http://localhost:8001/meetings"
    
    static let urlRequest1 = "http://localhost:8001/hiring-meetings/func1/"
    static let urlRequest2 = "http://localhost:8001/hiring-meetings/func2/"
    static let urlRequest3 = "http://localhost:8001/hiring-meetings/func3/"
    
    static let urlRequest6 = "http://localhost:8001/hiring-meetings/func4/"
    static let urlRequest10 = "http://localhost:8001/hiring-meetings/func5/"
    static let urlRequest11 = "http://localhost:8001/holidays/func6/"
}
 
