//
//  RequestViewController.swift
//  BD Project
//
//  Created by Nazar Lysak on 09.12.2021.
//

import UIKit

// MARK: - RequestViewController
class RequestViewController: UIViewController {

    @IBOutlet weak var requestLabel: UILabel!
    
    @IBOutlet weak var clientTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    
    @IBOutlet weak var clientPickerView: UIPickerView!
    @IBOutlet weak var fromAndToDatePickerView: UIDatePicker!
    
    @IBOutlet weak var infoTableView: UITableView!
    
    private let requestCellID = String(describing: RequestTableViewCell.self)
    var request: Requests!
    
    private var clients: [Clients] = []
    private var hiredFriends: [HiredFriends] = []
    private var meetings: [Meetings] = []
    
    private var request1: [Request1] = []
    private var request3: [Request3] = []
    private var request6: [Request6] = []
    private var request11: [Request11] = []
    
    private var selectedID: Int?
    private var hiredFriendID: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkRequests()
        
        requestLabel.text = request.rawValue
        
        infoTableView.register(UINib(nibName: requestCellID, bundle: nil), forCellReuseIdentifier: requestCellID)
        infoTableView.reloadData()
        
        showNamePicker()
        showDatePicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switch request {
            
        case .request1, .request8:
            loadClients()
            
        case .request2, .request3, .request7, .request11, .request12:
            loadHiredFriends()
            
        case .request4, .request5, .request6, .request9:
            break
        
        case .request10:
            loadClients()
            loadHiredFriends()
            
        default: break
        }
    }
    
    @IBAction private func onBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onFindButton(_ sender: Any) {
        loadRequestInformation()
    }
}

// MARK: - Private Methods
private extension RequestViewController {
    
    func showDatePicker() {
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneDatePicker));
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton, cancelButton], animated: false)
        
        fromDateTextField.inputAccessoryView = toolbar
        fromDateTextField.inputView = fromAndToDatePickerView
        
        toDateTextField.inputAccessoryView = toolbar
        toDateTextField.inputView = fromAndToDatePickerView
        
    }
    
    @objc func doneDatePicker() {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        if toDateTextField.isEditing {
            toDateTextField.text = formatter.string(from: fromAndToDatePickerView.date)
        } else if fromDateTextField.isEditing {
            fromDateTextField.text = formatter.string(from: fromAndToDatePickerView.date)
        }
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func showNamePicker() {
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(cancelNamePicker));
        
        toolbar.setItems([cancelButton], animated: false)
        
        clientTextField.inputAccessoryView = toolbar
        clientTextField.inputView = clientPickerView
        
        if request == .request10 || request == .request11 {
            amountTextField.inputAccessoryView = toolbar
            amountTextField.inputView = clientPickerView
        }
    }
    
    @objc func cancelNamePicker(){
        self.view.endEditing(true)
    }
    
    func checkRequests() {
        
        switch request {
            
        case .request1, .request8:
            clientTextField.placeholder = "Select client: "
            
        case .request2, .request3, .request7:
            clientTextField.placeholder = "Select hired friend: "
            
        case .request4, .request5, .request9:
            clientTextField.isHidden = true
            
        case .request6:
            clientTextField.isHidden = true
            amountTextField.isHidden = true
            fromDateTextField.isHidden = true
            toDateTextField.isHidden = true

        case .request10:
            clientTextField.placeholder = "Select client: "
            amountTextField.placeholder = "Select hired friend: "
            
        case .request11:
            clientTextField.placeholder = "Select hired friend 1: "
            amountTextField.placeholder = "Select hired friend 2: "
            fromDateTextField.isHidden = true
            toDateTextField.isHidden = true
            
        case .request12:
            clientTextField.placeholder = "Select hired friend: "
            amountTextField.isHidden = true
            fromDateTextField.isHidden = true
            toDateTextField.isHidden = true
            
        default: break
        }
    }
    
    func loadClients() {
        
        Rest().loadClients { clients, error in
            
            guard error == nil else {
                self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                return
            }

            guard let clients = clients else {
                self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            self.clients = clients
        }
    }
    
    func loadHiredFriends() {
        
        Rest().loadHiredFriends { hiredFriends, error in

            guard error == nil else {
                self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                return
            }

            guard let hiredFriends = hiredFriends else {
                self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                return
            }

            self.hiredFriends = hiredFriends
        }
    }
    
    func loadMeetings() {
        
        Rest().loadMeetings(completionHandler: { meetings, error in
            
            guard error == nil else {
                self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                return
            }

            guard let meetings = meetings else {
                self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            self.meetings = meetings
        })
    }
    
    func loadRequestInformation() {
        
        switch request {
            
        case .request1:
            
            guard let selectedID = selectedID else {
                self.present(AlertView().showAlert(title: "Error", message: "Please, select client!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let dateStart = fromDateTextField.text, !dateStart.isEmpty,
                  let dateEnd = toDateTextField.text, !dateEnd.isEmpty,
                  let amount = amountTextField.text, !amount.isEmpty
            else {
                
                self.present(AlertView().showAlert(title: "Error", message: "Check whether all fields entered!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let number = Int(amount) else {
                
                self.present(AlertView().showAlert(title: "Error", message: "You have to enter number!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            Rest().loadRequest1(clientId: selectedID, number: number, dateStart: dateStart, dateEnd: dateEnd) { request1, error in
                
                guard error == nil else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }

                guard let request1 = request1 else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }
                
                self.request1 = request1
                
                DispatchQueue.main.async {
                    self.infoTableView.reloadData()
                }
            }
            
        case .request2:
            
            guard let selectedID = selectedID else {
                self.present(AlertView().showAlert(title: "Error", message: "Please, select hired friend!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let dateStart = fromDateTextField.text, !dateStart.isEmpty,
                  let dateEnd = toDateTextField.text, !dateEnd.isEmpty,
                  let amount = amountTextField.text, !amount.isEmpty
            else {
                
                self.present(AlertView().showAlert(title: "Error", message: "Check whether all fields entered!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let number = Int(amount) else {
                
                self.present(AlertView().showAlert(title: "Error", message: "You have to enter number!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            Rest().loadRequest2(hiredFriendId: selectedID, number: number, dateStart: dateStart, dateEnd: dateEnd) { request1, error in
                
                guard error == nil else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }

                guard let request1 = request1 else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }
                
                self.request1 = request1
                
                DispatchQueue.main.async {
                    self.infoTableView.reloadData()
                }
            }
            
        case .request3:
            
            guard let selectedID = selectedID else {
                self.present(AlertView().showAlert(title: "Error", message: "Please, select hired friend!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let dateStart = fromDateTextField.text, !dateStart.isEmpty,
                  let dateEnd = toDateTextField.text, !dateEnd.isEmpty,
                  let amount = amountTextField.text, !amount.isEmpty
            else {
                
                self.present(AlertView().showAlert(title: "Error", message: "Check whether all fields entered!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let number = Int(amount) else {
                
                self.present(AlertView().showAlert(title: "Error", message: "You have to enter number!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            Rest().loadRequest3(hiredFriendId: selectedID, number: number, dateStart: dateStart, dateEnd: dateEnd) { request3, error in
                
                guard error == nil else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }

                guard let request3 = request3 else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }
                
                self.request3 = request3
                
                DispatchQueue.main.async {
                    self.infoTableView.reloadData()
                }
            }
            
        case .request4:
            break
            
        case .request5:
            break
            
        case .request6:
            
            Rest().loadRequest6 { request6, error in
                
                guard error == nil else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }

                guard let request6 = request6 else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }
                
                self.request6 = request6
                
                DispatchQueue.main.async {
                    self.infoTableView.reloadData()
                }
            }
            
        case .request7:
            break

        case .request8:
            break
            
        case .request9:
            break
            
        case .request10:
            
            guard let selectedID = selectedID,
                  let hiredFriendID = hiredFriendID
            else {
                self.present(AlertView().showAlert(title: "Error", message: "Please, select hired friend or client!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let dateStart = fromDateTextField.text, !dateStart.isEmpty,
                  let dateEnd = toDateTextField.text, !dateEnd.isEmpty
            else {
                
                self.present(AlertView().showAlert(title: "Error", message: "Check whether all fields entered!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            Rest().loadRequest10(hiredFriendId: hiredFriendID, clientId: selectedID, dateStart: dateStart, dateEnd: dateEnd) { request3, error in
                
                guard error == nil else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }

                guard let request3 = request3 else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }
                
                self.request3 = request3
                
                DispatchQueue.main.async {
                    self.infoTableView.reloadData()
                }
            }
            
        case .request11:
            
            guard let selectedID = selectedID,
                  let hiredFriendID = hiredFriendID
            else {
                self.present(AlertView().showAlert(title: "Error", message: "Please, select hired friend or client!", actionTitle: "OK"), animated: true, completion: nil)
                return
            }

            Rest().loadRequest11(startHiredFriendId: selectedID, endHiredFriendId: hiredFriendID) { request11, error in
                
                guard error == nil else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }

                guard let request11 = request11 else {
                    self.present(AlertView().showAlert(title: "Error", message: error, actionTitle: "OK"), animated: true, completion: nil)
                    return
                }
                
                self.request11 = request11
                
                DispatchQueue.main.async {
                    self.infoTableView.reloadData()
                }
            }
            
        case .request12:
            break
           
        default: break
        }
    }
}

// MARK: - UITableViewDelegate
extension RequestViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - UITableViewDataSource
extension RequestViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch request {
            
        case .request1, .request2:
            return request1.count
            
        case .request3:
            return request3.count
            
        case .request4, .request5, .request9:
            break
            
        case .request6:
            return request6.count

        case .request10:
            return request3.count
            
        case .request11:
            return request11.count
            
        case .request12:
            break
            
        default: break
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: requestCellID, for: indexPath) as! RequestTableViewCell
        
        switch request {
            
        case .request1, .request2:
            cell.update(text: request1[indexPath.row].first_name)
            
        case .request3:
            cell.update(text: request3[indexPath.row].name)
            
        case .request4, .request5, .request9:
            break
            
        case .request6:
            cell.update(text: request6[indexPath.row].month.split(separator: "T")[0] + "  -  " + request6[indexPath.row].count)

        case .request10:
            cell.update(text: request3[indexPath.row].name)
            
        case .request11:
            cell.update(text: "\(request11[indexPath.row].first_name) - \(request11[indexPath.row].date.split(separator: "T")[0])")
            
        case .request12:
            break
            
        default: break
        }
        
        return cell
    }
}

// MARK: - UIPickerViewDelegate, UIPickerViewDataSource
extension RequestViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch request {
            
        case .request1, .request8:  return clients.count
            
        case .request2, .request3, .request7, .request12:   return hiredFriends.count
            
        case .request4, .request5, .request6, .request9:    return 0

        case .request10:
            if clientTextField.isEditing {
                return clients.count
            } else if amountTextField.isEditing {
                return hiredFriends.count
            }
            
        case .request11:
            return hiredFriends.count
              
        default:    return 0
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        switch request {
            
        case .request1, .request8:  return "\(clients[row].firstName) \(clients[row].lastName)"
            
        case .request2, .request3, .request7, .request12:   return "\(hiredFriends[row].firstName) \(hiredFriends[row].lastName)"
            
        case .request4, .request5, .request6, .request9:    return ""

        case .request10:
            if clientTextField.isEditing {
                return "\(clients[row].firstName) \(clients[row].lastName)"
            } else if amountTextField.isEditing {
                return "\(hiredFriends[row].firstName) \(hiredFriends[row].lastName)"
            }
            
        case .request11:
            return "\(hiredFriends[row].firstName) \(hiredFriends[row].lastName)"
              
        default:    return ""
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch request {
            
        case .request1, .request8:
            if clientTextField.isEditing {
                clientTextField.text = "\(clients[row].firstName) \(clients[row].lastName)"
                selectedID = clients[row].id
            }
            
        case .request2, .request3, .request7, .request12:
            if clientTextField.isEditing {
                clientTextField.text = "\(hiredFriends[row].firstName) \(hiredFriends[row].lastName)"
                selectedID = hiredFriends[row].id
            }
            
        case .request4, .request5, .request6, .request9:    break

        case .request10:
            if clientTextField.isEditing {
                clientTextField.text = "\(clients[row].firstName) \(clients[row].lastName)"
                selectedID = clients[row].id
            } else if amountTextField.isEditing {
                amountTextField.text = "\(hiredFriends[row].firstName) \(hiredFriends[row].lastName)"
                hiredFriendID = hiredFriends[row].id
            }
            
        case .request11:
            if clientTextField.isEditing {
                clientTextField.text = "\(hiredFriends[row].firstName) \(hiredFriends[row].lastName)"
                selectedID = hiredFriends[row].id
            } else if amountTextField.isEditing {
                amountTextField.text = "\(hiredFriends[row].firstName) \(hiredFriends[row].lastName)"
                hiredFriendID = hiredFriends[row].id
            }
              
        default:    break
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

        switch textField {

        case clientTextField:
            clientPickerView.reloadAllComponents()
            clientPickerView.isHidden = false
            
        case fromDateTextField:   fromAndToDatePickerView.isHidden = false
        case toDateTextField:   fromAndToDatePickerView.isHidden = false
            
        case amountTextField:
            clientPickerView.reloadAllComponents()
            if request == .request10 || request == .request11 {
                clientPickerView.isHidden = false
            } else {
                break
            }

        default: break
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        switch textField {

        case clientTextField:   clientPickerView.isHidden = true
        case fromDateTextField:   fromAndToDatePickerView.isHidden = true
        case toDateTextField:   fromAndToDatePickerView.isHidden = true
        case amountTextField:
            if request == .request10 || request == .request11 {
                clientPickerView.isHidden = true
            } else {
                break
            }

        default: break
        }
    }
}
