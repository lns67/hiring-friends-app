//
//  RequestTableViewCell.swift
//  BD Project
//
//  Created by Nazar Lysak on 09.12.2021.
//

import UIKit

// MARK: - RequestTableViewCell
class RequestTableViewCell: UITableViewCell {

    @IBOutlet weak var requestView: UIView!
    @IBOutlet weak var requestLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        requestView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func update(text: String) {
        requestLabel.text = text
    }
}
