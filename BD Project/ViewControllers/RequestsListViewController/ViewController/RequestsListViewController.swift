//
//  RequestsListViewController.swift
//  BD Project
//
//  Created by Nazar Lysak on 09.12.2021.
//

import UIKit

// MARK: - RequestsListViewController
class RequestsListViewController: UIViewController {

    @IBOutlet weak var requestsTableView: UITableView!
    
    private let requestCellID = String(describing: RequestTableViewCell.self)
    private let requestsList: [Requests] = [.request1, .request2, .request3, .request4, .request5, .request6, .request7,
                                            .request8, .request9, .request10, .request11, .request12]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestsTableView.register(UINib(nibName: requestCellID, bundle: nil), forCellReuseIdentifier: requestCellID)
        requestsTableView.reloadData()
    }
}

// MARK: - UITableViewDelegate
extension RequestsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let requestVC = RequestViewController(nibName: String(describing: RequestViewController.self), bundle: nil)
        requestVC.request = requestsList[indexPath.row]
        
        self.present(requestVC, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension RequestsListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        requestsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: requestCellID, for: indexPath) as! RequestTableViewCell
        cell.update(text: requestsList[indexPath.row].rawValue)
        return cell
    }
}
